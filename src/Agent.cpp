
#include"Util.h"
#include "../include/Agent.h"
#include "../include/Util.h"
#include <algorithm>
using namespace std;

// creates the agent, sets the search algorithm of the agent
// do not change
Agent::Agent (char searchStr[]){
		if (!strcmp(searchStr,"BFS"))
				searchType = BFS;
		else if (!strcmp(searchStr,"DFS"))
				searchType = DFS;
		else if (!strcmp(searchStr,"UCS"))
				searchType = UCS;
		else if (!strcmp(searchStr,"A_STAR"))
				searchType = A_STAR;
		else if (!strcmp(searchStr,"RAND"))
				searchType = RAND;
		else 
				Util::Exit("Wrong search type/string in command line");
}

// sets the problem type
// do not change
void Agent::setProblemType (char problemType[]){
		if (!strcmp(problemType, "eat-food"))
				problem = new MazeProblem();
		else if (!strcmp(problemType, "visit-corners"))
				problem = new CornersProblem();
		else if (!strcmp(problemType, "eat-all-foods"))
				problem = new AllFoodsMazeProblem();
		else 
				Util::Exit("Wrong problem type in command line");
}

// reads layout fi
// do not change
void Agent::perceive (char envFile[], char weatherType[]){
		problem->readFromFile (envFile);
		problem->setCosts (weatherType);
		initState = problem->setInitState();
}

// calls the corresponding search functions
// do not change
void Agent::search(){
		if (searchType==RAND)
				randomSearch();
		else
				graphSearch();
}

// fake/no search for now, random actions
// do not change
void Agent::randomSearch(){
		Node  *initNode = new Node(initState);
		Node  *curNode = initNode;
		for (int i=0;i<100;i++){
				State *curState = curNode->getState();
				vector<int> nextActions;
				vector<State *> nextStates;
				problem->getSuccessorStateActionPairs (nextStates, nextActions, curState);
				int randActIndex = rand()%nextActions.size();
				if (nextStates[randActIndex]->isSameState(curState)){
						i--; continue;
				}

				Node *newNode     = new Node(nextStates[randActIndex]);
				newNode->setParentNode (curNode);
				newNode->action   = nextActions[randActIndex];
				newNode->depth    = curNode->depth++;
				newNode->pathCost = curNode->pathCost + 
						problem->getStateActionCost (curNode->getState(), newNode->action);
				
				curNode->childNodes.push_back(newNode);
				curNode = newNode;
				if (problem->isGoalState(curNode->getState())){
						break;
				}
		}
		solnNode = curNode;
}

// Prints the path (if msec>0) and prints out #
// do not change
void Agent::printSolution(int msec){
		if (solnNode==0)
				cout << "NO SOLUTION FOUND" << endl;
		else{
				printPath(solnNode,msec);
		}
		cout << "# of expanded nodes: " << problem->getExpandedCount() << endl;
		cout << "path cost:" << solnNode->pathCost << endl;
}


// A recursive function that prints the path starting from the final node
// do not change
void Agent::printPath(Node *curNode, int msec){
		if (msec==-1)
				return;
		if (curNode->getParentNode()==0){
				usleep(msec*400); system("clear");
				cout << "Weather: " << problem->getCostType() << endl << endl;
				problem->printState (initState);
				return;
		}
		printPath (curNode->getParentNode(),msec);
		usleep(msec*400); system("clear");
		cout << "Weather: " << problem->getCostType() << endl;
		cout << "ACTION:" << FSTARTBLU << problem->getActionName (curNode->action) << FEND << endl;
		problem->printState (curNode->getState());
}

// Implement for Q1-Q3 and use in the rest
void Agent::add2Fringe (Node *node){
		/********************* FILL-IN FROM HERE *********************/

        if(searchType != A_STAR){
            bool shouldAdd = true;
            std::vector<Node*>::iterator it;
            int i = 0;
            for (it=fringe.begin();it< fringe.end();it++,i++){
                if(fringe.at(i)->getState()->isSameState(node->getState())){
                    shouldAdd = false;
                }
            }
            if(shouldAdd){
                fringe.push_back(node);
            }

        }else{
            node->h_n = problem->heuristicFunc(node->getState());
            node->f_n = node->h_n + node->g_n;
            fringe.push_back(node);
        }
		/********************* FILL-IN UNTIL HERE *********************/
}

// Implement for Q1-Q3 and use in the rest
Node *Agent::removeFromFringe (){
		Node *node = 0;
		if (searchType == BFS){
		/********************* FILL-IN FROM HERE *********************/
			node = fringe.at(0);
			fringe.erase(fringe.begin());
		/********************* FILL-IN UNTIL HERE *********************/
		}else if (searchType == DFS){
		/********************* FILL-IN FROM HERE *********************/
			node = fringe.at(fringe.size()-1);
            fringe.pop_back();
		/********************* FILL-IN UNTIL HERE *********************/
		}else if (searchType == UCS ){
		/********************* FILL-IN FROM HERE *********************/
            std::vector<Node*>::iterator it;
            int index = 0;
            node = fringe.at(0);
            // find the minimum cost node
            for(it=fringe.begin();it<fringe.end();it++,index++){
                if(node->pathCost > fringe.at(index)->pathCost){
                    node = fringe.at(index);
                }
            }
            //delete the minimum cost node
            index = 0;
            for(it=fringe.begin();it<fringe.end();it++,index++){
                if(fringe.at(index)->getState()->isSameState(node->getState())){
                    fringe.erase(it);
                }
            }
            return node;
		/********************* FILL-IN UNTIL HERE *********************/
		}else if (searchType == A_STAR){
		/********************* FILL-IN FROM HERE *********************/
            std::vector<Node*>::iterator it;
            int index = 0;
            node = fringe.at(0);
            // find the minimum cost node
            for(it=fringe.begin();it<fringe.end();it++,index++){
                if(node->f_n > fringe.at(index)->f_n){
                    node = fringe.at(index);
                }
            }
            //delete the minimum cost node
            index = 0;
            for(it=fringe.begin();it<fringe.end();it++,index++){
                if(fringe.at(index)->getState()->isSameState(node->getState())){
                    fringe.erase(it);
                }
            }
		/********************* FILL-IN UNTIL HERE *********************/
		}
		return node;
}



// Implement for Q1 and use in the rest
Node *Agent::graphSearch(){
		Node *initNode = new Node(initState);
		initClosedList();
		add2Fringe (initNode);
		/********************* FILL-IN FROM HERE *********************/
		Node* currNode ;
		while(fringe.size() != 0){
			if (fringe.size() == 0){
					break;
				}
			currNode = removeFromFringe();
            if(problem->isGoalState(currNode->getState())){
                break;
            }
            add2ClosedList(currNode);
            currNode->expand(problem);
            std::vector<Node*>::iterator it;
            int index =0;
            for(it=currNode->childNodes.begin();it<currNode->childNodes.end();it++,index++){
                if(!isInClosedList(currNode->childNodes.at(index))) {
                    add2Fringe(currNode->childNodes.at(index));
                }
            }
		 }
        solnNode = currNode;
		/********************* FILL-IN UNTIL HERE *********************/
}

void Agent::add2ClosedList(Node *node){
		/********************* FILL-IN FROM HERE *********************/
		closedList.push_back(node);
		/********************* FILL-IN UNTIL HERE *********************/
}

void Agent::initClosedList(){
		/********************* FILL-IN FROM HERE *********************/
		closedList.clear();
		/********************* FILL-IN UNTIL HERE *********************/
}

bool Agent::isInClosedList(Node *node){
		/********************* FILL-IN FROM HERE *********************/
	std::vector<Node*>::iterator it;
	int i = 0;
	for (it=closedList.begin();it< closedList.end();it++,i++){
		if(closedList.at(i)->getState()->isSameState(node->getState())){
            return true;
        }
	}
	return false;

		/********************* FILL-IN UNTIL HERE *********************/
}

/******************* SEARCH TREE NODES **********************/

Node::Node(State* state){
						this->state = state;
						parentNode = 0;
						depth = 0;
						pathCost = 0;
						action = -1;
						f_n = g_n = h_n = 0;
}


void Node::expand(Problem *problem){
		// DO NOT REMOVE THIS CODE FROM HERE!
		if (parentNode){
				if (f_n < parentNode->f_n){
						problem->printState (parentNode->getState());
						problem->printState (state);
						Util::Exit("Inconsistent heuristics!");
				}
		}
		// DO NOT REMOVE THIS CODE UNTIL HERE!
		
		/********************* FILL-IN FROM HERE *********************/
        vector<int> nextActions;
        vector<State *> nextStates;
        problem->getSuccessorStateActionPairs (nextStates, nextActions, this->getState());
        for(int i =0; i< nextStates.size();i++){
            Node* newNode = new Node(nextStates[i]);
            newNode->setParentNode(this);
            newNode->action = nextActions[i];
            newNode->depth = this->depth++;
            newNode->pathCost = this->pathCost + problem->getStateActionCost(this->getState(),newNode->action);
            newNode->g_n = newNode->pathCost;
            this->childNodes.push_back(newNode);
        }
		/********************* FILL-IN UNTIL HERE *********************/
}


