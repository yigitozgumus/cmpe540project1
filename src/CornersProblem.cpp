
#include"Util.h"
#include "../include/MazeProblem.h"
#include "../include/CornersProblem.h"
#include <iostream>
using namespace std;

// extend for Q4 and use it for Q5
CornersState::CornersState (int agentPos, vector<int> foodPosList, vector<int> wallPosList)
        : MazeState(agentPos, foodPosList, wallPosList)
{
    /********************* FILL-IN FROM HERE *********************/
    std::map<int,bool>::iterator it;
    int index = 0;
    for(it=visitedCorners.begin();it != visitedCorners.end() and index <4;it++,index++){
        visitedCorners.insert(it,std::pair<int,bool>(index,false));
    }
    /********************* FILL-IN UNTIL HERE *********************/
}

// extend for Q4 and use it for Q5
bool CornersState::isSameState (State *state){
    /********************* FILL-IN FROM HERE *********************/
    CornersState* otherState = (CornersState*) state;
    return (this->agentPos == otherState->agentPos and
            this->cornersDiscovered == otherState->cornersDiscovered);
    /********************* FILL-IN UNTIL HERE *********************/
}


// Feel free to change, but not required
void CornersProblem::readFromFile (char fileName[]){
    MazeProblem::readFromFile (fileName);
}

// extend for Q4 and use it for Q5
State *CornersProblem::getNextState (State *curState, int action){
    int curAgentPos = ((CornersState *)curState)->agentPos;
    int dC, dR, curC, curR, nextC, nextR;
    getRowColFromPos (curR, curC, curAgentPos);
    getActionEffectRC (dR, dC, action);
    nextR = curR+dR;
    nextC = curC+dC;
    // if the next state is a wall or outside the maze, stay where you are
    if (maze[nextR][nextC] == WALL || nextR<0 || nextR>=nRows || nextC<0 || nextC>=nCols)
        return curState;
    int nextAgentPos = getPosFromRowCol (nextR, nextC);
    State *nextState = new CornersState (nextAgentPos, foodPosList, wallPosList);
    /********************* FILL-IN FROM HERE *********************/
    CornersState* corS = (CornersState*) curState;
    CornersState* nextS = (CornersState*) nextState;
    nextS->cornerList = corS->cornerList;
    nextS->cornersDiscovered = corS->cornersDiscovered;
    nextS->visitedCorners = corS->visitedCorners;
    nextState = (State*) nextS;
    /********************* FILL-IN UNTIL HERE *********************/
    return nextState;
}




// extend for Q4 and use it for Q5
State *CornersProblem::setInitState (){
    State *state = new CornersState(initAgentPos, foodPosList, wallPosList);
    CornersState *cornersState = (CornersState *) state;

    /********************* FILL-IN FROM HERE *********************/
    cornersState->cornersDiscovered = 0;
    cornersState->cornerList.push_back(getPosFromRowCol(1,1)); // Top Left
    cornersState->cornerList.push_back(getPosFromRowCol(1,nCols-2)); // Top Right
    cornersState->cornerList.push_back(getPosFromRowCol(nRows-2,1)); // Bottom Left
    cornersState->cornerList.push_back(getPosFromRowCol(nRows-2,nCols-2)); // Bottom Right
    state = (State*)cornersState;
    /********************* FILL-IN UNTIL HERE *********************/

    return state;
}

// extend for Q4 and use it for Q5
bool CornersProblem::isGoalState (State *curState){
    /********************* FILL-IN FROM HERE *********************/
    CornersState* current = ((CornersState*)curState);
    if (current->cornersDiscovered >= 15 ){ // 1 2 4 8
        return true;
    }else{
        std::vector<int>::iterator it;
        int index =0;
        for(it=current->cornerList.begin();it<current->cornerList.end();it++,index++){
            if(current->agentPos == current->cornerList.at(index)){
                current->cornersDiscovered += pow(2,index);
                std::map<int,bool>::iterator it2 = current->visitedCorners.find((const int &) index);
                if(it2 != current->visitedCorners.end()){it2->second = true;}
            }
        }
        return false;
    }
    /********************* FILL-IN UNTIL HERE *********************/
}



// Q5
double CornersProblem::heuristicFunc (State *state){
    /********************* FILL-IN FROM HERE *********************/
    if(isGoalState(state)){return 0;}
    //sum of all manhattan distances from unvisited corner to the agent and all unvisited corner pairs of
    // that corner.
    //find the agent position
    int r_a,c_a ;
    int agentPos = ((CornersState*)state)->agentPos;
    getRowColFromPos(r_a,c_a,agentPos);
    std::vector<int> corners = ((CornersState*)state)->cornerList;
    std::map<int,bool> visited = ((CornersState*)state)->visitedCorners;
    //find the agent position
    std::vector<int> h_n_vector;
    std::vector<int>::iterator it;
    int index = 0;
    for(it=corners.begin();it != corners.end();it++,index++){
        //for every unvisited corner
        if(!visited.find(index)->second){
            int r_c,c_c;
            getRowColFromPos(r_c,c_c,corners.at((unsigned long) index));
            int cost_corner = (abs(r_c-r_a) + abs(c_c-c_a));
            std::vector<int>::iterator it2;
            int index2 = 0;
            for(it2=corners.begin();it2 != corners.end();it2++,index2++){
                if(!visited.find(index2)->second){
                    int r_cc,c_cc;
                    getRowColFromPos(r_cc,c_cc,corners.at((unsigned long) index2));
                    cost_corner += (abs(r_cc-r_c)+ abs(c_cc - c_c));
                }
            }
            h_n_vector.push_back(cost_corner);

        }
    }
    //return distance

    return *std::min_element(h_n_vector.begin(),h_n_vector.end());
    /********************* FILL-IN UNTIL HERE *********************/
}




