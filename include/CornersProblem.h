#ifndef CORNERPROBLEM_H
#define CORNERPROBLEM_H

#include <map>
#include"MazeProblem.h"

// extend for Q4 and use it for Q5
class CornersState : public MazeState{
		public:
			// Q4-5
		/********************* FILL-IN FROM HERE *********************/
                std::vector<int> cornerList; // position of the corners
                int cornersDiscovered;  // 1 2 4 8 binary equivalent values = total 15
                std::map<int,bool> visitedCorners;// Heuristic Assistance
		/********************* FILL-IN UNTIL HERE *********************/
				CornersState (int agentPos, std::vector<int> foodPosList, std::vector<int> wallPosList);
				virtual bool isSameState (State *state);
};

class CornersProblem : public MazeProblem{
		public:
				void   readFromFile (char fileName[]);
				State* setInitState ();
				bool   isGoalState (State *curState);
				State* getNextState (State *curState, int action);
				double heuristicFunc (State *state);
};
#endif
